package com.ingenico;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import com.ingenico.backend.command.CreateTransferCmd;
import com.ingenico.service.TransferService;

import reactor.bus.Event;
import reactor.bus.EventBus;
import reactor.fn.Consumer;
import reactor.spring.context.config.EnableReactor;

@Configuration
@EnableReactor
public class ReactorConfiguration {

	@Autowired
	private EventBus eventBus;
	
	
	@Autowired
	private TransferService transferService;
	
	private Consumer<Event<CreateTransferCmd>> transfer() {
        return infoEvent -> {
        	try {
        		final CreateTransferCmd cmd = infoEvent.getData();
        		transferService.createTransfer(cmd);
			} catch (final Exception e) {
				e.printStackTrace();
			}
        };
    }

}
