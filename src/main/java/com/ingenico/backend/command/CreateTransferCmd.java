package com.ingenico.backend.command;

import org.reveno.atp.api.commands.CommandContext;

import com.ingenico.backend.entity.Account;
import com.ingenico.backend.entity.Transfer;

public class CreateTransferCmd {

	public final long fromAccountId;
	public final long toAccountId;
	public final double amount;

	public CreateTransferCmd(final long fromAccountId, final long toAccountId, 
								final double amount) {
		super();
		this.fromAccountId = fromAccountId;
		this.toAccountId = toAccountId;
		this.amount = amount;
	}
	
	public static class CreateTransferAction {
		
		public final long transferId;
		public final CreateTransferCmd info;
		
		public CreateTransferAction(final long transferId, final CreateTransferCmd info) {
			super();
			this.transferId = transferId;
			this.info = info;
		}
		
	}
	
	public static long handleCommand(final CreateTransferCmd cmd, final CommandContext ctx) {
		final long transferId = ctx.id(Transfer.class);
		ctx.executeTxAction(new CreateTransferAction(transferId, cmd)); 
		if (ctx.repo().has(Account.class, cmd.fromAccountId) &&
				ctx.repo().has(Account.class, cmd.toAccountId)) {
			final Account fromAccount = ctx.repo().get(Account.class, cmd.fromAccountId);
			if (fromAccount.balance >= cmd.amount) {
				ctx.executeTxAction(new DebitAccountCmd(fromAccount.id, cmd.amount));
				
				final Account toAccount = ctx.repo().get(Account.class, cmd.toAccountId);
				ctx.executeTxAction(new CreditAccountCmd(toAccount.id, cmd.amount));
			}
		}
		return transferId;
	}

}
