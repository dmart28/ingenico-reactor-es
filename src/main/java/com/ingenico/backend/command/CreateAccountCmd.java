package com.ingenico.backend.command;

import org.reveno.atp.api.commands.CommandContext;

import com.ingenico.backend.entity.Account;

public class CreateAccountCmd {

	public final String name;
	public final double initialBalance;

	public CreateAccountCmd(final String name, final double initialBalance) {
		this.name = name;
		this.initialBalance = initialBalance;
	}

	public static class CreateAccountAction {
		public final CreateAccountCmd info;
		public final long id;

		public CreateAccountAction(final CreateAccountCmd info, final long id) {
			this.info = info;
			this.id = id;
		}
	}
	
	public static long handleCommand(final CreateAccountCmd cmd, final CommandContext ctx) {
		final long accountId = ctx.id(Account.class);
		if (cmd.initialBalance > 0) {
			ctx.executeTxAction(new CreateAccountCmd.CreateAccountAction(cmd, accountId));
        }
        return accountId;
	}
}
