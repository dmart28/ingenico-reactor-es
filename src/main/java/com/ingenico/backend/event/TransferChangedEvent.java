package com.ingenico.backend.event;

public class TransferChangedEvent {
	
	public final long accountId;

    public TransferChangedEvent(final long accountId) {
        this.accountId = accountId;
    }
	
}
