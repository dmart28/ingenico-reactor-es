package com.ingenico.backend.tx.action;

import org.reveno.atp.api.transaction.TransactionContext;

import com.ingenico.backend.command.DebitAccountCmd;
import com.ingenico.backend.entity.Account;
import com.ingenico.util.Utils;

public class DebitAccountTxAction {
	
	public static void handleTxAction(final DebitAccountCmd cmd, final TransactionContext ctx) {
		
		 ctx.repo().remap(cmd.accountId, Account.class, 
				 			(id, e) -> e.reduceBalance(Utils.toLong(cmd.amount)));
	}
	
}
