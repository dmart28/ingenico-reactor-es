package com.ingenico.backend.tx.action;

import org.reveno.atp.api.transaction.TransactionContext;

import com.ingenico.backend.command.CreateAccountCmd.CreateAccountAction;
import com.ingenico.backend.entity.Account;
import com.ingenico.util.Utils;

public class CreateAccountTxAction {
	public static void handleTxAction(final CreateAccountAction action, final TransactionContext ctx) {
		 ctx.repo().store(action.id, 
				 new Account(action.id, action.info.name, Utils.toLong(action.info.initialBalance)));
	}
}