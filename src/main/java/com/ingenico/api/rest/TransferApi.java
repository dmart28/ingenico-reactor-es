package com.ingenico.api.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ingenico.api.rest.dto.CreateTransferDTO;
import com.ingenico.backend.command.CreateTransferCmd;
import com.ingenico.service.TransferService;

@RestController
@RequestMapping("/api/v1/transfers")
public class TransferApi {
	
//	@Autowired
//    private EventBus eventBus;
	
	@Autowired
	private TransferService transferService;
	
	@RequestMapping(method = RequestMethod.POST)
	public long transfer(@RequestBody final CreateTransferDTO cmdDTO) {
		
		final CreateTransferCmd cmd = new CreateTransferCmd(cmdDTO.getFromAccountId(), 
				cmdDTO.getToAccountId(), cmdDTO.getAmount());
//		eventBus.notify("transfer", Event.wrap(cmd));
		final long transferId = transferService.createTransfer(cmd);
		return transferId;
//		return 0;
	}
	
}
