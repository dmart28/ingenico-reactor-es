package com.ingenico.api.rest.dto;

public class CreateTransferDTO {

	private long fromAccountId;
	private long toAccountId;
	private double amount;

	public CreateTransferDTO() {
		super();
	}

	public CreateTransferDTO(final long fromAccountId, final long toAccountId, final double amount) {
		super();
		this.fromAccountId = fromAccountId;
		this.toAccountId = toAccountId;
		this.amount = amount;
	}

	public long getFromAccountId() {
		return fromAccountId;
	}

	public void setFromAccountId(final long fromAccountId) {
		this.fromAccountId = fromAccountId;
	}

	public long getToAccountId() {
		return toAccountId;
	}

	public void setToAccountId(final long toAccountId) {
		this.toAccountId = toAccountId;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(final double amount) {
		this.amount = amount;
	}

}
